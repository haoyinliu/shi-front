import request from '@/utils/request'

export function collect(data) {
    return request({
        url: '/shi/collect/save',
        method: 'post',
        data
    })
}