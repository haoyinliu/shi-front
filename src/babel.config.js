const plugins = [
    'babel-plugin-transform-es2015-arrow-functions',
  ]
  module.exports = {
    env: {
      development: {
        sourceMaps: true,
        retainLines: true,
      },
    },
    presets: [
      [
        '@vue/app', {
          useBuiltIns: 'entry',
        },
      ],
    ],
    plugins: plugins,
  }
  
  