import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Demo from '@/components/Demo'
import Shi from '@/components/Shi'
import Data from '@/components/Data'
import Login from '@/components/Login'
import MyCollect from '@/pages/MyCollections'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/demo',
      name: 'demo',
      component: Demo
    },
    {
      path: '/',
      name: 'shi',
      component: Shi
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/myCollect',
      name: 'myCollect',
      component: MyCollect
    },
    {
      path: '/data',
      name: 'data',
      component: Data
    }
  ]
})
