module.exports = {
    devServer: { 
      open: true,
      host: '127.0.0.1', // 本地域名/ip地址
      port: '8080', // 端口号
      proxy: { // 配置跨域
         '/back': {
          target: 'http://127.0.0.1:9090/', // 需要代理的地址
          ws:true,//代理websocked
          secure: false, // 如果是不是https接口，可以不配置这个参数
          changeOrigin: true, // 允许跨域
          pathRewrite: {
            // 如果本身的接口地址就有"/api"这种通用前缀，也就是说https://www.exaple.com/api，就可以把pathRewrite删掉，如果没有则加上
            '^/shi': '/shi',
          },
        },
      },
    },
    configureWebpack: { // webpack 配置
     devtool: 'source-map',
    },
}